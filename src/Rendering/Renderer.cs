﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TurnBased.App.Types;

namespace TurnBased.App.Rendering
{
    public class Renderer
    {
        private readonly ContentFactory _contentFactory;
        private readonly SpriteBatch _spriteBatch;

        public Renderer(SpriteBatch spriteBatch, ContentFactory contentFactory)
        {
            _spriteBatch = spriteBatch;
            _contentFactory = contentFactory;
        }

        public void Begin(Matrix viewMatrix)
        {
            _spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, viewMatrix);
        }

        public void Render(Rectangle rectangle, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Blank(), rectangle, color);
        }

        public void Render(Position position, IndexSize size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Blank(), new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height), color);
        }

        public void Render(string textureName, Position position, IndexSize size, float rotation)
        {
            var rectangle = new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height);
            _spriteBatch.Draw(_contentFactory.Load(textureName), rectangle, null, Color.White, rotation, Vector2.Zero, SpriteEffects.None, 1);
        }

        public void Render(string textureName, Position position, IndexSize size, Position sourcePosition, IndexSize sourceSize, float rotation)
        {
            var rectangle = new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height);
            var soureceRectangle = new Rectangle((int)sourcePosition.X, (int)sourcePosition.Y, sourceSize.Width, sourceSize.Height);
            _spriteBatch.Draw(_contentFactory.Load(textureName), rectangle, soureceRectangle, Color.White, rotation, Vector2.Zero, SpriteEffects.None, 1);
        }

        public void End()
        {
            _spriteBatch.End();
        }
    }
}