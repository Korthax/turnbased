﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TurnBased.App.Rendering
{
    public class ContentFactory
    {
        private readonly Dictionary<string, Texture2D> _textures;
        private readonly ContentManager _contentManager;

        public static ContentFactory From(GraphicsDevice graphicsDevice, ContentManager contentManager)
        {
            var texture = new Texture2D(graphicsDevice, 1, 1);
            texture.SetData(new[] { Color.White });
            return new ContentFactory(contentManager, texture);
        }

        private ContentFactory(ContentManager contentManager, Texture2D blankTexture)
        {
            _contentManager = contentManager;
            _textures = new Dictionary<string, Texture2D> { { "", blankTexture } };
        }

        public Texture2D Blank()
        {
            return _textures[""];
        }

        public Texture2D Load(string name)
        {
            if (!_textures.ContainsKey(name))
                _textures.Add(name, _contentManager.Load<Texture2D>(name));
            return _textures[name];
        }
    }
}