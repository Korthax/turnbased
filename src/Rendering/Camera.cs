﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TurnBased.App.Rendering
{
    public class Camera
    {
        private readonly Viewport _viewport;
        private readonly Vector2 _origin;
        public Vector2 Position;
        public float Zoom;

        public Camera(Viewport viewport)
        {
            _viewport = viewport;
            _origin = new Vector2(viewport.Width / 2.0f, viewport.Height / 2.0f);
            Zoom = 1.0f;
            Position = Vector2.Zero;
        }

        public Matrix GetViewMatrix(Vector2 parallax)
        {
            return Matrix.CreateTranslation(new Vector3(-Position * parallax, 0.0f)) *
                   Matrix.CreateTranslation(new Vector3(-_origin, 0.0f)) *
                   Matrix.CreateScale(Zoom, Zoom, 1) *
                   Matrix.CreateTranslation(new Vector3(_origin, 0.0f));
        }

        public void LookAt(Vector2 location)
        {
            Position = location - _origin;
        }

        public void Update(GameTime gameTime)
        {
            
        }
    }
}