﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using Microsoft.Xna.Framework.Input;

namespace TurnBased.App.Input
{
    public interface IInputDetector
    {
        IObservable<InputEvent> OnKeysPressed { get; }
        IObservable<InputEvent> OnKeysDown { get; }

        void Update();
    }

    public class InputEvent
    {
        public ModifierKeys Modifiers { get; set; }
        public HashSet<Keys> KeySet { get; set; }

        public static InputEvent From(HashSet<Keys> keys)
        {
            var modifiers = ModifierKeys.None;

            if(keys.Contains(Keys.LeftAlt) || keys.Contains(Keys.RightAlt))
                modifiers |= ModifierKeys.Alt;

            if(keys.Contains(Keys.LeftShift) || keys.Contains(Keys.RightShift))
                modifiers |= ModifierKeys.Shift;

            if (keys.Contains(Keys.LeftControl) || keys.Contains(Keys.RightControl))
                modifiers |= ModifierKeys.Control;

            return new InputEvent
            {
                Modifiers = modifiers,
                KeySet = keys
            };
        }
    }

    [Flags]
    public enum ModifierKeys
    {
        None = 0,
        Alt = 1,
        Shift = 2,
        Control = 4,
    }

    public class InputDetector : IInputDetector
    {
        public IObservable<InputEvent> OnKeysPressed => _onKeysPressed;
        public IObservable<InputEvent> OnKeysDown => _onKeysDown;

        private readonly Subject<InputEvent> _onKeysPressed;
        private readonly Subject<InputEvent> _onKeysDown;

        private KeyboardState _oldKeyboardState;

        public InputDetector()
        {
            _oldKeyboardState = Keyboard.GetState();
            _onKeysPressed = new Subject<InputEvent>();
            _onKeysDown = new Subject<InputEvent>();
        }

        public void Update()
        {
            var newKeyboardState = Keyboard.GetState();

            var downKeys = new HashSet<Keys>(newKeyboardState.GetPressedKeys());
            var oldDownKeys = _oldKeyboardState.GetPressedKeys().ToList();
            var pressedKeys = new HashSet<Keys>(oldDownKeys.Where(x => !downKeys.Contains(x)));

            _oldKeyboardState = newKeyboardState;

            _onKeysDown.OnNext(InputEvent.From(downKeys));
            _onKeysPressed.OnNext(InputEvent.From(pressedKeys));
        }
    }
}