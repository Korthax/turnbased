using System;
using Microsoft.Xna.Framework;

namespace TurnBased.App.Types
{
    public class Vector
    {
        public static Vector Zero => new Vector(0, 0);
        public float X;
        public float Y;

        public Vector(float x, float y)
        {
            X = x;
            Y = y;
        }

        public Vector(Position position)
        {
            X = position.X;
            Y = position.Y;
        }

        public Vector(Vector2 position)
        {
            X = position.X;
            Y = position.Y;
        }

        public static Vector Normalize(Vector a)
        {
            var magnitude = (float)Math.Sqrt(a.X * a.X + a.Y * a.Y);
            return new Vector(a.X / magnitude, a.Y / magnitude);
        }

        public static float DotProduct(Vector a, Vector b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static float DotProduct(Vector a, Position b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static float DotProduct(Position a, Vector b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static float Distance(Vector a, Vector b)
        {
            return (float)Math.Sqrt(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));
        }

        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y);
        }

        public static Vector operator -(Vector a)
        {
            return new Vector(-a.X, -a.Y);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        public static Vector operator -(Vector a, Position b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        public static Vector operator *(Vector a, float b)
        {
            return new Vector(a.X * b, a.Y * b);
        }

        public static Vector operator *(Vector a, int b)
        {
            return new Vector(a.X * b, a.Y * b);
        }

        public static Vector operator *(Vector a, double b)
        {
            return new Vector((float)(a.X * b), (float)(a.Y * b));
        }

        public static bool operator ==(Vector a, Vector b)
        {
            return Math.Abs(a.X - b.X) < float.Epsilon && Math.Abs(a.Y - b.Y) < float.Epsilon;
        }

        public static bool operator !=(Vector a, Vector b)
        {
            return Math.Abs(a.X - b.X) > float.Epsilon || Math.Abs(a.Y - b.Y) > float.Epsilon;
        }

        public override string ToString()
        {
            return $"{X}, {Y}";
        }

        public static Vector Negate(Vector axis)
        {
            return new Vector(axis.X * -1, axis.Y * -1);
        }


        public bool Equals(Vector other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            return obj.GetType() == GetType() && Equals((Vector)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }
    }
}