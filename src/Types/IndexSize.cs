namespace TurnBased.App.Types
{
    public class IndexSize
    {
        public static IndexSize Zero => new IndexSize(0, 0); 
        public int Width { get; set; }
        public int Height { get; set; }

        public IndexSize(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }
}