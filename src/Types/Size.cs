using System;

namespace TurnBased.App.Types
{
    public class Size
    {
        public static Size Zero => new Size(0, 0);

        public float Width { get; }
        public float Height { get; }

        public Size(float width, float height)
        {
            Width = width;
            Height = height;
        }

        public float Max()
        {
            return Width > Height ? Width : Height;
        }

        public static Size operator *(Size size, float scale)
        {
            return new Size(size.Width * scale, size.Height * scale);
        }

        public static Size operator /(Size size, float scale)
        {
            return new Size(size.Width / scale, size.Height / scale);
        }

        public static Size operator *(Size size, Vector scale)
        {
            return new Size(size.Width * scale.X, size.Height * scale.Y);
        }

        public static bool operator ==(Size a, Size b)
        {
            return Math.Abs(a.Width - b.Width) < float.Epsilon && Math.Abs(a.Height - b.Height) < float.Epsilon;
        }

        public static bool operator !=(Size a, Size b)
        {
            return !(a == b);
        }

        public bool Equals(Size other)
        {
            return Width.Equals(other.Width) && Height.Equals(other.Height);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            return obj is Size size && Equals(size);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Width.GetHashCode() * 397) ^ Height.GetHashCode();
            }
        }
    }
}