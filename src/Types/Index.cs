using System;
using Microsoft.Xna.Framework;

namespace TurnBased.App.Types
{
    public class Index
    {
        public int X { get; set; }
        public int Y { get; set; }

        public static Index From(Vector2 vector)
        {
            return new Index((int)vector.X, (int)vector.Y);
        }

        public static Index From(double x, double y, Position centre)
        {
            var dx = x > centre.X ? Math.Ceiling(x) : Math.Floor(x);
            var dy = x > centre.Y ? Math.Ceiling(y) : Math.Floor(y);
            return new Index((int)dx, (int)dy);
        }

        public Index(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(Index a, Index b)
        {
            if (ReferenceEquals(null, a) && ReferenceEquals(null, b))
                return true;

            if (ReferenceEquals(null, a) || ReferenceEquals(null, b))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Index a, Index b)
        {
            return !(a == b);
        }

        protected bool Equals(Index other)
        {
            return Y == other.Y && X == other.X;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((Index)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Y * 397) ^ X;
            }
        }
    }
}