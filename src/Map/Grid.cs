using Microsoft.Xna.Framework;
using TurnBased.App.Rendering;
using TurnBased.App.Types;

namespace TurnBased.App.Map
{
    public class Grid
    {
        public int this[int x, int y]
        {
            get => _tiles[y][x];
            set => _tiles[y][x] = value;
        }

        public int MapHeight => _size.Height;
        public int MapWidth => _size.Width;

        public float WorldHeight => _size.Height * _tileSize.Height;
        public float WorldWidth => _size.Width * _tileSize.Width;

        private readonly int[][] _tiles;
        private readonly IndexSize _tileSize;
        private readonly IndexSize _size;

        public static Grid From(IndexSize size, IndexSize tileSize)
        {
            var tiles = new int[size.Height][];
            for (var i = 0; i < size.Height; i++)
                tiles[i] = new int[size.Width];

            return new Grid(tiles, size, tileSize);
        }

        private Grid(int[][] tiles, IndexSize size, IndexSize tileSize)
        {
            _tiles = tiles;
            _size = size;
            _tileSize = tileSize;
        }

        public bool IsInBounds(int x, int y)
        {
            return x >= 0 && x < _size.Width && y >= 0 && y < _size.Height;
        }

        public void DrawTo(Renderer renderer, Position offset)
        {
            for (var x = 0; x < _size.Width; x++)
            {
                for (var y = 0; y < _size.Height; y++)
                {
                    renderer.Render(new Position(x * _tileSize.Width + offset.X, y * _tileSize.Height + offset.Y), _tileSize, this[x, y] == 0 ? Color.Black : Color.Red);
                }
            }
        }
    }
}