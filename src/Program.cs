﻿namespace TurnBased.App
{
    public class Program
    {
        public static void Main()
        {
            using (var game = new Game1())
                game.Run();
        }
    }
}