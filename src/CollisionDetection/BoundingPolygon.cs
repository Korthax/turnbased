﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TurnBased.App.CollisionDetection
{
    public class BoundingPolygon : IEnumerable<Polygon>
    {
        private readonly List<Polygon> _polygons;
        private readonly Guid _identifier;

        public BoundingPolygon(List<Polygon> polygons, Guid identifier)
        {
            _polygons = polygons;
            _identifier = identifier;
        }

        public IEnumerator<Polygon> GetEnumerator()
        {
            return _polygons.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) 
                return false;

            if (ReferenceEquals(this, obj)) 
                return true;

            return obj.GetType() == GetType() && Equals((BoundingPolygon) obj);
        }

        protected bool Equals(BoundingPolygon other)
        {
            return _identifier.Equals(other._identifier);
        }

        public override int GetHashCode()
        {
            return _identifier.GetHashCode();
        }
    }
}