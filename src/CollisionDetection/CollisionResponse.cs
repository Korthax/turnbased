﻿using TurnBased.App.Types;

namespace TurnBased.App.CollisionDetection
{
    public class CollisionResponse
    {
        public static CollisionResponse None => new CollisionResponse(Vector.Zero, 0, false);

        public readonly bool HasOccurred;
        public readonly Vector Inverse;
        public readonly Vector Result;
        public readonly float Depth;

        public CollisionResponse(Vector translationAxis, float minIntervalDistance) : this(translationAxis, minIntervalDistance, true) { }

        private CollisionResponse(Vector translationAxis, float minIntervalDistance, bool hasOccured)
        {
            Result = Vector.Normalize(translationAxis) * minIntervalDistance;
            Inverse = Vector.Normalize(translationAxis) * -minIntervalDistance;
            Depth = minIntervalDistance;
            HasOccurred = hasOccured;
        }
    }
}