﻿using System;
using System.Collections.Generic;
using System.Linq;
using TurnBased.App.Rendering;
using TurnBased.App.Types;

namespace TurnBased.App.CollisionDetection
{
    public class Polygon
    {
        public List<Position> Points;
        public List<Vector> Edges;
        public Vector Center;

        public static Polygon From(params Position[] points)
        {
            return From(points.ToList());
        }

        public static Polygon From(List<Position> points)
        {
            var edges = new List<Vector>();
            for (var i = 0; i < points.Count; i++)
            {
                var p1 = points[i];
                var p2 = i + 1 >= points.Count ? points[0] : points[i + 1];
                edges.Add(new Vector(p2 - p1));
            }

            var totalX = points.Sum(x => x.X);
            var totalY = points.Sum(x => x.Y);

            var center = new Vector(totalX / points.Count, totalY / points.Count);
            return new Polygon(points.ToList(), edges, center);
        }

        private Polygon(List<Position> points, List<Vector> edges, Vector center)
        {
            Points = points;
            Edges = edges;
            Center = center;
        }

        public bool Contains(Position position)
        {
            int i, j;
            var result = false;
            for (i = 0, j = Points.Count - 1; i < Points.Count; j = i++)
            {
                if ((Points[i].Y > position.Y) != (Points[j].Y > position.Y) && position.X < (Points[j].X - Points[i].X) * (position.Y - Points[i].Y) / (Points[j].Y - Points[i].Y) + Points[i].X)
                    result = !result;
            }

            return result;
        }

        public bool SharesAnEdgeWith(Polygon polygon)
        {
            var result = Points.Where(x => polygon.Points.Contains(x)).ToList();

            return result.Count == 2;
        }

        public void Render(Renderer renderer)
        {
            for (var x = 0; x < Points.Count; x++)
            {
                var end = x + 1 >= Points.Count ? Points[0] : Points[x + 1];
                var edge = end - Points[x];
                var angle = (float)Math.Atan2(edge.Y, edge.X);
                renderer.Render(null, Points[x], new IndexSize((int)edge.Length(), 1), angle);
            }
        }

        public static bool MergeWith(Polygon p, Polygon polygon, ref List<Polygon> ears)
        {
            var result = p.Points.Where(x => polygon.Points.Contains(x)).ToList();
            if (result.Count != 2)
                return false;

            var mergedPolygon = From(polygon.Points.Union(p.Points).ToList());

            if (!IsOnTheLeft(mergedPolygon, result[0]))
                return false;

            if (!IsOnTheLeft(mergedPolygon, result[1]))
                return false;

            ears.Remove(polygon);
            ears.Remove(p);
            ears.Add(mergedPolygon);
            return true;
        }

        private static bool IsOnTheLeft(Polygon mergedPolygon, Position position)
        {
            Position previous;
            if (!mergedPolygon.TryGetPreviousPoint(position, out previous))
                return false;

            Position next;
            if (!mergedPolygon.TryGetNextPoint(position, out next))
                return false;

            return Math.Sign((previous.X - next.X)*(position.Y - next.Y) - (previous.Y - next.Y)*(position.X - next.X)) > 0;
        }

        private bool TryGetPreviousPoint(Position position, out Position result)
        {
            for (var i = 0; i < Points.Count; i++)
            {
                if (!position.Equals(Points[i])) 
                    continue;

                result = i - 1 < 0 ? Points[Points.Count - 1] : Points[i - 1];
                return true;
            }

            result = null;
            return false;
        }

        private bool TryGetNextPoint(Position position, out Position result)
        {
            for (var i = 0; i < Points.Count; i++)
            {
                if (!position.Equals(Points[i])) 
                    continue;

                result = i + 1 >= Points.Count ? Points[0] : Points[i + 1];
                return true;
            }

            result = null;
            return false;
        }
    }
}