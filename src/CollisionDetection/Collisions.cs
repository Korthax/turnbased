﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace TurnBased.App.CollisionDetection
{
    public class Collisions<T>
    {
        public decimal Count => _collisions.Count;

        private readonly ConcurrentBag<KeyValuePair<T, CollisionResponse>> _collisions;

        public Collisions()
        {
            _collisions = new ConcurrentBag<KeyValuePair<T, CollisionResponse>>();
        }

        public void Add(T item, CollisionResponse collisionResponse)
        {
            if(collisionResponse.HasOccurred)
                _collisions.Add(new KeyValuePair<T, CollisionResponse>(item, collisionResponse));
        }

        public void ResolveAll(Action<T, CollisionResponse> action)
        {
            foreach (var collision in _collisions)
                action(collision.Key, collision.Value);
        }
    }
}