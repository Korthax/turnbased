﻿using System;
using TurnBased.App.Map;
using TurnBased.App.Types;

namespace TurnBased.App.CollisionDetection
{
    internal static class CollisionMapDetector
    {
        public static CollisionResponse CheckForWorldCollision(Grid collisionMap, BoundingPolygon bounds)
        {
            foreach (var polygon in bounds)
            {
                foreach (var position in polygon.Points)
                {
                    var x = (int) Math.Floor(position.X);
                    var y = (int) Math.Floor(position.Y);

                    if (x < 0 || y < 0 || x > collisionMap.WorldWidth - 1 || y > collisionMap.WorldHeight - 1)
                    {
                        if (x < 0)
                            x = 0;
                        else if (x > collisionMap.WorldWidth - 1)
                            x = (int)(collisionMap.WorldWidth - 1);

                        if (y < 0)
                            y = 0;
                        else if (y > collisionMap.WorldHeight - 1)
                            y = (int)(collisionMap.WorldHeight - 1);

                        return new CollisionResponse(new Vector(x, y), 1);
                    }

                    if (collisionMap[x, y] > 0)
                        continue;

                    var currentPosition = new Vector(x, y);

                    for (var i = 1; i <= 4; i++)
                    {
                        var smallestDistance = float.MaxValue;
                        var newPosition = new Vector(x, y);
                        var foundNewPosition = false;

                        for (var x1 = -i; x1 <= i; x1++)
                        {
                            for (var y1 = -i; y1 <= i; y1++)
                            {
                                var newX = x + x1;
                                var newY = y + y1;

                                if (newX < 0 || newY < 0 || newX > collisionMap.WorldWidth - 1 || newY > collisionMap.WorldHeight - 1)
                                    continue;

                                if (collisionMap[newX, newY] == 0)
                                    continue;

                                var potentialPosition = new Vector(newX, newY);
                                var distance = Math.Abs(Vector.Distance(potentialPosition, currentPosition));
                                if (distance > smallestDistance)
                                    continue;

                                foundNewPosition = true;
                                smallestDistance = distance;
                                newPosition = potentialPosition;
                            }
                        }

                        if (foundNewPosition)
                            return new CollisionResponse(new Vector(new Position(newPosition.X, newPosition.Y) - position), 1);
                    }
                }
            }

            return CollisionResponse.None;
        }
    }
}