﻿using System;
using TurnBased.App.Types;

namespace TurnBased.App.CollisionDetection
{
    internal static class SeparatingAxisDetector
    {
        public static CollisionResponse PolygonCollision(BoundingPolygon boundsA, BoundingPolygon boundsB)
        {
            if (boundsA.Equals(boundsB))
                return CollisionResponse.None;

            var collision = CollisionResponse.None;
            foreach (var polygonA in boundsA)
            {
                foreach (var polygonB in boundsB)
                {
                    var result = CheckForCollision(polygonA, polygonB);
                    if (result.HasOccurred && result.Depth > collision.Depth)
                        collision = result;
                }
            }

            return collision;
        }

        private static CollisionResponse CheckForCollision(Polygon polygonA, Polygon polygonB)
        {
            var edgeCountA = polygonA.Edges.Count;
            var edgeCountB = polygonB.Edges.Count;
            var minIntervalDistance = float.PositiveInfinity;
            var translationAxis = Vector.Zero;

            for (var edgeIndex = 0; edgeIndex < edgeCountA + edgeCountB; edgeIndex++)
            {
                var edge = edgeIndex < edgeCountA ? polygonA.Edges[edgeIndex] : polygonB.Edges[edgeIndex - edgeCountA];

                var axis = Vector.Normalize(new Vector(-edge.Y, edge.X));

                ProjectPolygon(axis, polygonA, out float minA, out float maxA);
                ProjectPolygon(axis, polygonB, out float minB, out float maxB);

                var intervalDistance = IntervalDistance(minA, maxA, minB, maxB);
                if (intervalDistance >= 0)
                    return CollisionResponse.None;

                intervalDistance = Math.Abs(intervalDistance);
                if (intervalDistance >= minIntervalDistance)
                    continue;

                minIntervalDistance = intervalDistance;
                translationAxis = axis;

                var distance = polygonA.Center - polygonB.Center;
                if (Vector.DotProduct(distance, translationAxis) < 0)
                    translationAxis = -translationAxis;
            }

            return new CollisionResponse(translationAxis, minIntervalDistance);
        }

        private static float IntervalDistance(float minA, float maxA, float minB, float maxB)
        {
            if (minA < minB)
                return minB - maxA;

            return minA - maxB;
        }

        private static void ProjectPolygon(Vector axis, Polygon polygon, out float min, out float max)
        {
            var dotProduct = Vector.DotProduct(axis, polygon.Points[0]);

            min = dotProduct;
            max = dotProduct;

            foreach (var point in polygon.Points)
            {
                dotProduct = Position.DotProduct(point, axis);
                if (dotProduct < min)
                    min = dotProduct;
                else if (dotProduct > max)
                    max = dotProduct;
            }
        }
    }
}