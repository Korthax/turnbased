﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using TurnBased.App.Rendering;
using TurnBased.App.Types;

namespace TurnBased.App.Animations
{
    public interface IAnimationLibrary
    {
        void SetDirection(AnimationDirection direction);
        void Add(string name, IAnimationBranch directionalAnimation);
        void Play(string name, float speed, int priority);
        void Update(GameTime gameTime);
        void SetDefaultTransition(string name, float speed);
        void Draw(string textureName, Position position, IndexSize size, Renderer renderer);
    }

    public class AnimationLibrary : IAnimationLibrary
    {
        private readonly Dictionary<string, IAnimationBranch> _animations;

        private AnimationDirection _direction;
        private Transition _currentTransition;
        private Transition _defaultAnimation;
        private Animation _currentAnimation;

        public AnimationLibrary(float speed) : this("none", speed, new SingleAnimationBranch(Animation.None(IndexSize.Zero))) { }

        public AnimationLibrary(string name, float speed, IAnimationBranch directionalAnimationBranch)
        {
            _animations = new Dictionary<string, IAnimationBranch> { { name, directionalAnimationBranch } };
            _direction = AnimationDirection.Down;
            _defaultAnimation = _currentTransition = new Transition { Name = name, Speed = speed };
            _currentAnimation = _animations[name][_direction];
        }

        public void SetDefaultAnimation(AnimationDirection direction)
        {
            _direction = direction;
        }

        public void SetDirection(AnimationDirection direction)
        {
            _direction = direction;
        }

        public void Add(string name, IAnimationBranch directionalAnimation)
        {
            _animations.Add(name, directionalAnimation);
        }

        public void Play(string name, float speed, int priority)
        {
            if (priority >= _currentTransition.Priority)
                _currentTransition = new Transition { Name = name, Speed = speed, Priority = priority };
        }

        public void Update(GameTime gameTime)
        {
            _currentAnimation = _currentAnimation.TransitionTo(_animations[_currentTransition.Name][_direction], _currentTransition.Speed);
            _currentAnimation = _currentAnimation.Update(gameTime);

            if (_currentAnimation.IsDone() && _currentAnimation.IsLoopable())
                _currentAnimation = _currentAnimation.Restart();
            else if (_currentAnimation.IsDone() && !_currentAnimation.IsLoopable() || _defaultAnimation.Priority >= _currentTransition.Priority)
                _currentTransition = _defaultAnimation;
        }

        public void SetDefaultTransition(string name, float speed)
        {
            _defaultAnimation = new Transition { Name = name, Speed = speed, Priority = 0 };

            if(_currentTransition.Priority != 0)
                return;

            _currentTransition = new Transition { Name = name, Speed = speed, Priority = 0 };
            _currentAnimation = _currentAnimation.TransitionTo(_animations[_currentTransition.Name][_direction], _currentTransition.Speed);
        }

        public void Draw(string textureName, Position position, IndexSize size, Renderer renderer)
        {
            _currentAnimation.Draw(textureName, position, size, renderer);
        }
    }
}