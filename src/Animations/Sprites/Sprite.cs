﻿using Microsoft.Xna.Framework;
using TurnBased.App.Rendering;
using TurnBased.App.Types;

namespace TurnBased.App.Animations.Sprites
{
    public interface ISprite
    {
        void Update(GameTime gameTime);
        void AddAnimation(string name, IAnimationBranch animationBranch);
        void SetDirection(AnimationDirection animationDirection);
        void PlayAnimation(string animationName, float speed, int priority);
        void SetDefaultAnimation(string name, float speed);
        void RenderTo(Position position, Renderer renderer);
    }

    public class Sprite : ISprite
    {
        private readonly IAnimationLibrary _animationLibrary;
        private readonly string _textureName;
        private readonly IndexSize _size;

        public Sprite(IndexSize size, string textureName, IAnimationLibrary animationLibrary)
        {
            _size = size;
            _animationLibrary = animationLibrary;
            _textureName = textureName;
        }

        public void RenderTo(Position position, Renderer renderer)
        {
            _animationLibrary.Draw(_textureName, position, _size, renderer);
        }

        public void Update(GameTime gameTime)
        {
            _animationLibrary.Update(gameTime);
        }

        public void AddAnimation(string name, IAnimationBranch animationBranch)
        {
            _animationLibrary.Add(name, animationBranch);
        }

        public void SetDirection(AnimationDirection animationDirection)
        {
            _animationLibrary.SetDirection(animationDirection);
        }

        public void PlayAnimation(string animationName, float speed, int priority)
        {
            _animationLibrary.Play(animationName, speed, priority);
        }

        public void SetDefaultAnimation(string name, float speed)
        {
            _animationLibrary.SetDefaultTransition(name, speed);
        }
    }
}