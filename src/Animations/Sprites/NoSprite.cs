﻿using Microsoft.Xna.Framework;
using TurnBased.App.Rendering;
using TurnBased.App.Types;

namespace TurnBased.App.Animations.Sprites
{
    public class NoSprite : ISprite
    {
        public void RenderTo(Position position, Renderer renderer)
        {
        }

        public void Update(GameTime gameTime)
        {
        }

        public void AddAnimation(string name, IAnimationBranch animationBranch)
        {
        }

        public void SetDirection(AnimationDirection animationDirection)
        {
        }

        public void PlayAnimation(string animationName, float speed, int priority)
        {
        }

        public void SetDefaultAnimation(string name, float speed)
        {
        }
    }
}