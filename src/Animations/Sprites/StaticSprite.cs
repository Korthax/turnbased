﻿using Microsoft.Xna.Framework;
using TurnBased.App.Rendering;
using TurnBased.App.Types;

namespace TurnBased.App.Animations.Sprites
{
    public class StaticSprite : ISprite
    {
        private readonly string _textureName;
        private readonly IndexSize _size;

        public StaticSprite(string textureName, IndexSize size)
        {
            _textureName = textureName;
            _size = size;
        }

        public void RenderTo(Position position, Renderer renderer)
        {
            renderer.Render(_textureName, position, _size, 0);
        }

        public void Update(GameTime gameTime)
        {
        }

        public void AddAnimation(string name, IAnimationBranch animationBranch)
        {
        }

        public void SetDirection(AnimationDirection animationDirection)
        {
        }

        public void PlayAnimation(string animationName, float speed, int priority)
        {
        }

        public void SetDefaultAnimation(string name, float speed)
        {
        }
    }
}