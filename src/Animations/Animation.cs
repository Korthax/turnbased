﻿using Microsoft.Xna.Framework;
using TurnBased.App.Rendering;
using TurnBased.App.Types;

namespace TurnBased.App.Animations
{
    public class Animation
    {
        private readonly AnimationDirection _direction;
        private readonly float _frameInterval;
        private readonly int _numberOfFrames;
        private readonly IndexSize _size;
        private readonly Vector _offset;
        private readonly string _name;
        private readonly bool _loop;
        private int _currentFrame;
        private float _timer;

        public static Animation None(IndexSize size)
        {
            return new Animation(AnimationDirection.Down, 1, 0, true, Vector.Zero, size, "none");
        }

        public Animation(AnimationDirection direction, int numberOfFrames, float frameInterval, bool loop, Vector offset, IndexSize size, string name)
        {
            _direction = direction;
            _numberOfFrames = numberOfFrames;
            _frameInterval = frameInterval;
            _loop = loop;
            _offset = offset;
            _size = size;
            _name = name;
        }

        internal Animation Update(GameTime gameTime)
        {
            _timer += gameTime.ElapsedGameTime.Milliseconds;

            if (_frameInterval > 0 && _timer >= _frameInterval)
            {
                _currentFrame++;
                _timer -= _frameInterval;
            }

            if (_currentFrame >= _numberOfFrames && _loop)
                _currentFrame = 0;

            if (IsDone() && IsLoopable())
                return Restart();

            return this;
        }

        internal Animation StartNew(float speed)
        {
            return new Animation(_direction, _numberOfFrames, speed / _numberOfFrames, _loop, _offset, _size, _name);
        }

        internal bool IsDone()
        {
            return (_currentFrame >= _numberOfFrames) && !_loop;
        }

        internal bool IsLoopable()
        {
            return _loop;
        }

        internal Animation TransitionTo(Animation animation, float speed)
        {
            if (_name == animation._name && _direction == animation._direction)
                return this;

            return animation.StartNew(speed);
        }

        internal Animation Restart()
        {
            return new Animation(_direction, _numberOfFrames, _frameInterval, _loop, _offset, _size, _name);
        }

        internal void Draw(string textureName, Position position, IndexSize size, Renderer renderer)
        {
            renderer.Render(textureName, position, size, new Position((int) (_offset.X + (_currentFrame * _size.Width)), _offset.Y), _size, 0);
        }
    }
}