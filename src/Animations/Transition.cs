﻿namespace TurnBased.App.Animations
{
    internal class Transition
    {
        public string Name { get; set; }
        public float Speed { get; set; }
        public int Priority { get; set; }
    }
}