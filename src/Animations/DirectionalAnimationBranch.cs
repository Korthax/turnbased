﻿using System.Collections.Generic;

namespace TurnBased.App.Animations
{
    public interface IAnimationBranch
    {
        Animation this[AnimationDirection direction] { get; }
    }

    public class DirectionalAnimationBranch : IAnimationBranch
    {
        public Animation this[AnimationDirection direction] => _animations[direction];

        private readonly Dictionary<AnimationDirection, Animation> _animations;

        public DirectionalAnimationBranch(Animation right, Animation left, Animation down, Animation up)
        {
            _animations = new Dictionary<AnimationDirection, Animation>
            {
                { AnimationDirection.Right, right },
                { AnimationDirection.Left, left },
                { AnimationDirection.Down, down },
                { AnimationDirection.Up, up }
            };
        }
    }

    public class SingleAnimationBranch : IAnimationBranch
    {
        public Animation this[AnimationDirection direction] => _animation;

        private readonly Animation _animation;

        public SingleAnimationBranch(Animation animation)
        {
            _animation = animation;
        }
    }
}