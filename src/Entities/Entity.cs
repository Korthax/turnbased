﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using TurnBased.App.Animations.Sprites;
using TurnBased.App.Input;

namespace TurnBased.App.Entities
{
    public interface IEntity
    {
        void Move();
    }

    public class Entity : IEntity
    {
        private readonly IMovementController _movementController;
        private readonly ISprite _sprite;

        public Entity(IMovementController movementController, ISprite sprite)
        {
            _movementController = movementController;
            _sprite = sprite;
        }

        public void Initialise(KeyBindings keyBindings)
        {

        }

        public void Move()
        {
            
        }

        public void SubscribeTo(IInputDetector inputDetector)
        {
            inputDetector.OnKeysPressed.Subscribe(inputEvent =>
            {
                
            });
        }
    }

    public enum GameAction
    {
        None = 0,
        Move = 1
    }

    public class KeyBindings
    {
        public TriggerStyle TriggerStyle { get; set; }
        public Dictionary<Keys, GameAction> GameActions { get; set; }

        public void ApplyTo(IInputDetector inputDetector, IEntity entity)
        {
            foreach(var (key, action) in GameActions)
            {
                
            }
        }
    }

    public static class DeconstuctionExtensions
    {
        public static void Deconstruct(this KeyValuePair<Keys, GameAction> result, out Keys key, out GameAction action)
        {
            key = result.Key;
            action = result.Value;
        }
    }

    public class MouseBindings
    {
        public TriggerStyle TriggerStyle { get; set; }
        public Dictionary<Keys, GameAction> GameActions { get; set; }

        public void ApplyTo(IInputDetector inputDetector)
        {
            
        }
    }

    public enum TriggerStyle
    {
        None = 0,
        Normal = 1,
        Quick = 2
    }

    public interface IMovementController
    {
    }

    public class EntityContainer
    {
        public void Add(Entity entity)
        {
            
        }
    }
}